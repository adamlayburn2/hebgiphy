﻿using System;
using System.Collections.Generic;

namespace HEBGiphy
{
    public partial class Users
    {
        public Users()
        {
            Giphs = new HashSet<Giphs>();
            Labels = new HashSet<Labels>();
            Sessions = new HashSet<Sessions>();
        }

        public string Userid { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public string Email { get; set; }
        public DateTime Createdon { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Giphs> Giphs { get; set; }
        public virtual ICollection<Labels> Labels { get; set; }
        public virtual ICollection<Sessions> Sessions { get; set; }
    }
}
