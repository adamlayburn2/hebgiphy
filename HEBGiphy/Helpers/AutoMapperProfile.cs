﻿using AutoMapper;
using HEBGiphy.Models;

namespace HEBGiphy.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Users, UsersModel>();
            CreateMap<Sessions, SessionsModel>();
        }
    }
}