﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace HEBGiphy
{
    public partial class Labels
    {
        public string Labelid { get; set; }
        [JsonIgnore]
        public string Giphid { get; set; }
        [JsonIgnore]
        public string Userid { get; set; }
        public string Label { get; set; }
        [JsonIgnore]
        public virtual Giphs Giph { get; set; }
        [JsonIgnore]
        public virtual Users User { get; set; }
    }
}
