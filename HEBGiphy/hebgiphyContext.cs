﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace HEBGiphy
{
    public partial class hebgiphyContext : DbContext
    {
        public hebgiphyContext()
        {
        }

        public hebgiphyContext(DbContextOptions<hebgiphyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Giphs> Giphs { get; set; }
        public virtual DbSet<Labels> Labels { get; set; }
        public virtual DbSet<Sessions> Sessions { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.Entity<Giphs>(entity =>
            {
                entity.HasKey(e => e.Giphid)
                    .HasName("giphs_pkey");

                entity.ToTable("giphs");

                entity.HasIndex(e => e.Userid)
                    .HasName("fki_giphs.userId_user.userId");

                entity.Property(e => e.Giphid)
                    .HasColumnName("giphid")
                    .HasMaxLength(50);

                entity.Property(e => e.Giphurl)
                    .HasColumnName("giphurl")
                    .HasMaxLength(1000);

                entity.Property(e => e.Createdon)
                    .HasColumnName("createdon")
                    .HasColumnType("timestamp with time zone");

                entity.Property(e => e.Deletedon)
                    .HasColumnName("deletedon")
                    .HasColumnType("timestamp with time zone");

                entity.Property(e => e.Userid)
                    .IsRequired()
                    .HasColumnName("userid")
                    .HasMaxLength(50);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Giphs)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("giphs.userId_user.userId");
            });

            modelBuilder.Entity<Labels>(entity =>
            {
                entity.HasKey(e => e.Labelid)
                    .HasName("labels_pkey");

                entity.ToTable("labels");

                entity.HasIndex(e => e.Giphid)
                    .HasName("fki_label.giphId_giphs.giphId");

                entity.Property(e => e.Labelid)
                    .HasColumnName("labelid")
                    .HasMaxLength(50);

                entity.Property(e => e.Userid)
                    .IsRequired()
                    .HasColumnName("userid")
                    .HasMaxLength(50);

                entity.Property(e => e.Giphid)
                    .IsRequired()
                    .HasColumnName("giphid")
                    .HasMaxLength(50);

                entity.Property(e => e.Label).HasColumnName("label");

                entity.HasOne(d => d.Giph)
                    .WithMany(p => p.Labels)
                    .HasForeignKey(d => d.Giphid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("label.giphId_giphs.giphId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Labels)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("label.userId_user.userId");
            });

            modelBuilder.Entity<Sessions>(entity =>
            {
                entity.HasKey(e => e.Sessionid)
                    .HasName("Session_pkey");

                entity.ToTable("sessions");

                entity.HasIndex(e => e.Userid)
                    .HasName("fki_sessionId.userId_user.userId");

                entity.Property(e => e.Sessionid)
                    .HasColumnName("sessionid")
                    .HasMaxLength(50);

                entity.Property(e => e.Createdon)
                    .HasColumnName("createdon")
                    .HasColumnType("timestamp with time zone")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.Userid)
                    .IsRequired()
                    .HasColumnName("userid")
                    .HasMaxLength(50);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Sessions)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sessionId.userId_user.userId");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Userid)
                    .HasName("User_pkey");

                entity.ToTable("users");

                entity.Property(e => e.Userid)
                    .HasColumnName("userid")
                    .HasMaxLength(50);

                entity.Property(e => e.Createdon)
                    .HasColumnName("createdon")
                    .HasColumnType("timestamp with time zone");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.First)
                    .HasColumnName("first")
                    .HasMaxLength(255);

                entity.Property(e => e.Last)
                    .HasColumnName("last")
                    .HasMaxLength(255);

                entity.Property(e => e.Password).HasColumnName("password");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
