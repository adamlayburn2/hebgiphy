using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HEBGiphy.Models;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using HEBGiphy;
using AutoMapper;

namespace HEBGiphy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly hebgiphyContext _context;
        private IMapper _mapper;

        public RegisterController(hebgiphyContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        
        // POST: api/Register
        [HttpPost]
        public async Task<ActionResult<Users>> Post([FromBody] RegisterModel value)
        {
         
            Users u = new Users();
            u.Userid = Guid.NewGuid().ToString(); // server assigned UUID
            u.Email = value.email;
            u.Createdon = DateTime.Now; // server timestamps
            u.First = value.first;
            u.Last = value.last;
            u.Password = Helpers.Hash.CreateMD5(u.Userid + value.password); // hash is salted via a UUID and pw

            if (UsersExists(u.Email))
            {
                return Conflict();
            }

            _context.Users.Add(u);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UsersExists(u.Email))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(_mapper.Map<UsersModel>(u));
        }

        private bool UsersExists(string id)
        {
            return _context.Users.Any(e => e.Email == id);
        }
    }
}
