using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HEBGiphy;
using AutoMapper;
using HEBGiphy.Models;

namespace HEBGiphy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly hebgiphyContext _context;
        private IMapper _mapper;

        public UsersController(hebgiphyContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Users/UUID
        [HttpGet("{id}")]
        public async Task<ActionResult<UsersModel>> GetUsers(string id)
        {
            var sessionID = Request.Headers["hebsession"];

            //map all sessions to userid, all data restricted by userid
            var sessions = await _context.Sessions.FindAsync(sessionID);

            if (sessions == null)
            {
                return Unauthorized();
            }

            var users = _context.Users.Where(e => e.Userid == sessions.Userid && e.Userid == id).First();

            if (users == null)
            {
                return NotFound();
            }

            return _mapper.Map<UsersModel>(users);
        }
    }
}
