using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HEBGiphy;
using AutoMapper;
using HEBGiphy.Models;

namespace HEBGiphy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        private readonly hebgiphyContext _context;
        private IMapper _mapper;

        public SessionsController(hebgiphyContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Sessions/UUID
        [HttpGet("{id}")]
        public async Task<ActionResult<SessionsModel>> GetSessions(string id)
        {
            var sessions = await _context.Sessions.FindAsync(id);

            if (sessions == null)
            {
                return NotFound();
            }

            return _mapper.Map<SessionsModel>(sessions);
        }

        /*
        // DELETE: api/Sessions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Sessions>> DeleteSessions(string id)
        {
            var sessions = await _context.Sessions.FindAsync(id);
            if (sessions == null)
            {
                return NotFound();
            }

            _context.Sessions.Remove(sessions);
            await _context.SaveChangesAsync();

            return sessions;
        }*/
    }
}
