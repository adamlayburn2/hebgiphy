using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HEBGiphy;
using HEBGiphy.Models;

namespace HEBGiphy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LabelsController : ControllerBase
    {
        private readonly hebgiphyContext _context;

        public LabelsController(hebgiphyContext context)
        {
            _context = context;
        }

        // POST: api/Labels
        [HttpPost]
        public async Task<ActionResult<Labels>> PostLabels([FromBody] LabelModel labels)
        {
            var sessionID = Request.Headers["hebsession"];

            //map all sessions to userid, all data restricted by userid
            var sessions = await _context.Sessions.FindAsync(sessionID);

            if (sessions == null)
            {
                return Unauthorized();
            }

            //giph must exist as well
            var giphs = _context.Giphs.Where(e => e.Userid == sessions.Userid && e.Giphid == labels.Giphid).FirstOrDefault();

            if (giphs == null)
            {
                return NotFound();
            }

            Labels l = new Labels();
            l.Userid = sessions.Userid;
            l.Labelid = Guid.NewGuid().ToString(); // server assigned UUID
            l.Label = labels.Label;
            l.Giphid = labels.Giphid;
            _context.Labels.Add(l);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                return Conflict();
            }

            return Ok(l);
        }

        // DELETE: api/Labels/UUID
        [HttpDelete("{id}")]
        public async Task<ActionResult<Labels>> DeleteLabels(string id)
        {
            var sessionID = Request.Headers["hebsession"];

            //map all sessions to userid, all data restricted by userid
            var sessions = await _context.Sessions.FindAsync(sessionID);

            if (sessions == null)
            {
                return Unauthorized();
            }

            var labels = _context.Labels.Where(e => e.Userid == sessions.Userid && e.Labelid == id).FirstOrDefault();
            if (labels == null)
            {
                return NotFound();
            }

            _context.Labels.Remove(labels); // hard delete
            await _context.SaveChangesAsync();

            return labels;
        }
    }
}
