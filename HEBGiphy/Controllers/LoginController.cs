using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HEBGiphy.Models;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using HEBGiphy;
using AutoMapper;

namespace HEBGiphy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly hebgiphyContext _context;
        private IMapper _mapper;

        public LoginController(hebgiphyContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // POST: api/Login
        [HttpPost]
        public async Task<ActionResult<SessionsModel>> Post([FromBody] LoginModel value)
        {
            var users = _context.Users.FirstOrDefault(e => e.Email == value.email);

            if (users == null)
            {
                return NotFound();
            }
            if (users.Password == Helpers.Hash.CreateMD5(users.Userid + value.password)) // hash is salted via a UUID and pw
            {
                Sessions s = new Sessions();
                s.Userid = users.Userid;
                s.Sessionid = Guid.NewGuid().ToString(); // server assigned sessionId
                s.Createdon = DateTime.Now; // server timestamp

                _context.Sessions.Add(s);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    return BadRequest();
                }
                return _mapper.Map<SessionsModel>(s); ;
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
