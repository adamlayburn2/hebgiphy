using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HEBGiphy;
using HEBGiphy.Models;

namespace HEBGiphy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GiphsController : ControllerBase
    {
        private readonly hebgiphyContext _context;

        public GiphsController(hebgiphyContext context)
        {
            _context = context;
        }

        // GET: api/Giphs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Giphs>>> GetGiphs()
        {
            var sessionID = Request.Headers["hebsession"];

            //map all sessions to userid, all data restricted by userid
            var sessions = await _context.Sessions.FindAsync(sessionID);

            if (sessions == null)
            {
                return Unauthorized();
            }

            // return giphs with labels that are not deleted with most recent first
            var giphs = _context.Giphs.Include(d => d.Labels).Where(e => e.Userid == sessions.Userid && e.Deletedon == null)
                .OrderByDescending(g => g.Createdon)
                .ToList();

            if (giphs == null)
            {
                return NotFound();
            }

            return giphs;
        }

        // GET: api/Giphs/UUID
        [HttpGet("{id}")]
        public async Task<ActionResult<Giphs>> GetGiphs(string id)
        {
            var sessionID = Request.Headers["hebsession"];

            //map all sessions to userid, all data restricted by userid
            var sessions = await _context.Sessions.FindAsync(sessionID);

            if (sessions == null)
            {
                return Unauthorized();
            }

            var giphs = _context.Giphs.Where(e => e.Userid == sessions.Userid && e.Giphid == id).FirstOrDefault();

            if (giphs == null)
            {
                return NotFound();
            }

            return giphs;
        }

        
        // POST: api/Giphs
        [HttpPost]
        public async Task<ActionResult<Giphs>> addGiph([FromBody] GiphModel giph)
        {
            var sessionID = Request.Headers["hebsession"];

            //map all sessions to userid, all data restricted by userid
            var sessions = await _context.Sessions.FindAsync(sessionID);

            if (sessions == null)
            {
                return Unauthorized();
            }

            Giphs g = new Giphs();
            g.Giphid = Guid.NewGuid().ToString(); // server assigned UUID
            g.Createdon = DateTime.Now; // server timestamps
            g.Giphurl = giph.Giphurl;
            g.Userid = sessions.Userid; // validated session userid
            _context.Giphs.Add(g);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                return Conflict();
            }

            return Ok(g);
        }

        // DELETE: api/Giphs/UUID
        [HttpDelete("{id}")]
        public async Task<ActionResult<Giphs>> DeleteGiphs(string id)
        {
            var sessionID = Request.Headers["hebsession"];

            //map all sessions to userid, all data restricted by userid
            var sessions = await _context.Sessions.FindAsync(sessionID);

            if (sessions == null)
            {
                return Unauthorized();
            }

            var giphs = _context.Giphs.Where(e => e.Userid == sessions.Userid && e.Giphid == id).FirstOrDefault();

            if (giphs == null)
            {
                return NotFound();
            }

            giphs.Deletedon = DateTime.Now;  // soft delete

            _context.Giphs.Update(giphs);
            await _context.SaveChangesAsync();

            return giphs;
        }
    }
}
