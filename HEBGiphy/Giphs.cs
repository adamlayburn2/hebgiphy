﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace HEBGiphy
{
    public partial class Giphs
    {
        public Giphs()
        {
            Labels = new HashSet<Labels>();
        }

        public string Giphid { get; set; }
        public string Userid { get; set; }
        public string Giphurl { get; set; }
        public DateTimeOffset Createdon { get; set; }
        public DateTimeOffset? Deletedon { get; set; }

        public virtual Users User { get; set; }
        
        public virtual ICollection<Labels> Labels { get; set; }
    }
}
