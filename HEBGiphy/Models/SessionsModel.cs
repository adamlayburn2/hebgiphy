﻿using System;
namespace HEBGiphy.Models
{
    public class SessionsModel
    {
        /** 
         * Reduced input/output model for Sessions
         */
        public string Sessionid { get; set; }
        public string Userid { get; set; }
        public DateTime Createdon { get; set; }
    }
}
