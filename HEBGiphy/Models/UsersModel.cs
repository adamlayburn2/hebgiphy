﻿using System;
namespace HEBGiphy.Models
{
    public class UsersModel
    {
        public UsersModel()
        { }

        /** 
         * Reduced input/output model for Users
         */
        public string Userid { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public string Email { get; set; }
        public DateTime Createdon { get; set; }
    }
}
