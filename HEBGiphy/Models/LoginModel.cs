﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HEBGiphy.Models
{
    /** 
     * Reduced input/output model for Login
     */
    public class LoginModel
    {
        [Required]
        public string email { get; set; }

        [Required]
        public string password { get; set; }
    }
}