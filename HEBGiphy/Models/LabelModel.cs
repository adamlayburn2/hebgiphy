﻿using System;
namespace HEBGiphy.Models
{
    /** 
     * Reduced input/output model for Labels
     */
    public class LabelModel
    {
        public string Giphid { get; set; }
        public string Label { get; set; }
    }
}
