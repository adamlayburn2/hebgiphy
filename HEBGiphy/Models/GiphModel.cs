﻿using System;
namespace HEBGiphy.Models
{
    /** 
     * Reduced input/output model for Giphs
     */
    public class GiphModel
    {
        public string Giphurl { get; set; }
    }
}
