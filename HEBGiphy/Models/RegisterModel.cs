﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HEBGiphy.Models
{
    /** 
     * Reduced input/output model for Register
     */
    public class RegisterModel
    {
        [Required]
        public string first { get; set; }

        [Required]
        public string last { get; set; }

        [Required]
        public string email { get; set; }

        [Required]
        public string password { get; set; }
    }
}
