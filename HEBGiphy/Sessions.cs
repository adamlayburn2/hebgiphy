﻿using System;
using System.Collections.Generic;

namespace HEBGiphy
{
    public partial class Sessions
    {
        public string Sessionid { get; set; }
        public string Userid { get; set; }
        public DateTime Createdon { get; set; }

        public virtual Users User { get; set; }
    }
}
