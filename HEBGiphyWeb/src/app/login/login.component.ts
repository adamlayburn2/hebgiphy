import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, RoutesRecognized } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { withLatestFrom } from 'rxjs/operators';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  failedLogin: boolean;
  failureMessage: string;

  constructor(private router: Router,
             private formBuilder: FormBuilder,
             private route: ActivatedRoute,
             private loginService: LoginService,
             public snackBar: MatSnackBar,) { }

  ngOnInit() {
       this.loginForm = this.formBuilder.group({
           email: ['', Validators.required],
           password: ['', Validators.required]
       });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  login() {
      this.submitted = true;
      this.failedLogin = false;
      this.failureMessage = null;

      if (this.loginForm.invalid) {
          return; // stop here if form is invalid
      }

      this.loginService.logIn(this.f.email.value, this.f.password.value).subscribe(user => {
        if(user.error){
          this.failedLogin = true;
          if(user.error['error'] == 'PasswordChangeRequired'){
             this.failureMessage = 'Your Account Requires a Password Change.';
          }
          else if(user.error['error'] == 'Disabled'){
             this.failureMessage = 'Your Account Has Been Disabled.';
          }
          else if(user.error.title){
            this.snackBar.open("Authentication Failed: " + user.error.title, "close", { duration: 2000, panelClass:['snackbar-error'] });
          }
          else if(user.message){
            this.snackBar.open("Authentication Failed: " + user.message, "close", { duration: 2000, panelClass:['snackbar-error'] });
          }
          else{
            this.snackBar.open("Authentication Failed:  Try again later.", "close", { duration: 2000, panelClass:['snackbar-error'] });
          }
        }
        else{
          if(user != null){
            this.router.navigate(['/list']);
          }
          else{
            this.failedLogin = true;
          }
        }
      });
  }
}
