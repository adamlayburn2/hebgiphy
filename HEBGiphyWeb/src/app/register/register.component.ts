import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, RoutesRecognized } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { withLatestFrom } from 'rxjs/operators';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  failedRegister: boolean;
  failureMessage: string;
  done = false;

  constructor(private router: Router,
             private formBuilder: FormBuilder,
             private route: ActivatedRoute,
             private registerService: RegisterService,
             public snackBar: MatSnackBar) { }

  ngOnInit() {
       this.registerForm = this.formBuilder.group({
           first: ['', Validators.required],
           last: ['', Validators.required],
           email: ['', Validators.required],
           password: ['', Validators.required]
       });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  register() {
      this.done = false;
      this.submitted = true;
      this.failedRegister = false;
      this.failureMessage = null;

      if (this.registerForm.invalid) {
          return; // stop here if form is invalid
      }

      this.registerService.register(this.f.first.value, this.f.last.value, this.f.email.value, this.f.password.value).subscribe(user => {
        if(user.error){
          this.failedRegister = true;
          if(user.error.title){
            this.snackBar.open("Sorry but registration failed: " + user.error.title, "close", { duration: 2000, panelClass:['snackbar-error'] });
          }
          else if(user.message){
            this.snackBar.open("Sorry but registration failed: " + user.message, "close", { duration: 2000, panelClass:['snackbar-error'] });
          }
          else{
            this.snackBar.open("Sorry but registration failed: Please try again later...", "close", { duration: 2000, panelClass:['snackbar-error'] });
          }
        }
        else{
          if(user != null){
            this.snackBar.open("Account Created Successfully!", "close", { duration: 2000, panelClass:['snackbar-success'] });
            this.done = true;
          }
          else{
            this.failedRegister = true;
          }
        }

      });
  }

}
