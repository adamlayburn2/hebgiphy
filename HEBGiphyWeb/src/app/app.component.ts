import { Component } from '@angular/core';
import { LoginService } from './services/login.service';
import { FavoritesSearchService } from './services/favorites-search.service';
import { GiphySearchService } from './services/giphy-search.service';
import { Router, ActivatedRoute, ParamMap, RoutesRecognized } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'HEBGiphyWeb';

  constructor(private loginService: LoginService, private router: Router,
              private favoritesSearchService: FavoritesSearchService,
              private giphySearchService: GiphySearchService) { }

   logout(){
    // todo: actually DELETE the session by id
    this.loginService.logout();
    this.favoritesSearchService.reset();
    this.giphySearchService.reset();

    this.router.navigate(['login']);

   }
}
