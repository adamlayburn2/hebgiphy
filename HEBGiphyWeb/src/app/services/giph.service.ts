import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { FavoritesSearchService } from './favorites-search.service';

@Injectable({
  providedIn: 'root'
})
export class GiphService {

  constructor(private http: HttpClient, private favoritesSearchService: FavoritesSearchService) { }

  /*
  * Soft delete a GIF from your favorites
  */
  delete(id):Observable<any> {
    return this.http.delete<any>('/api/giphs/' + id)
    .pipe(
       tap( res =>{
        this.favoritesSearchService.search().subscribe( res => {});
        return of(true);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
  * Add a GIF to your favorites
  */
  add(url):Observable<any> {
    return this.http.post<any>('/api/giphs/', { "Giphurl": url })
    .pipe(
       tap( res =>{
        this.favoritesSearchService.search().subscribe( res => {});
        return of(true);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
  * Hard delete the label on a GIF
  */
  deleteLabel(id):Observable<any> {
    return this.http.delete<any>('/api/labels/' + id)
    .pipe(
       tap( res =>{
        this.favoritesSearchService.search().subscribe( res => {});
        return of(true);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
  * Add a label to your favorite GIF
  */
  addLabel(img, value):Observable<any> {
    return this.http.post<any>('/api/labels/', { "giphid": img.giphid, "label": value })
    .pipe(
       tap( res =>{
        this.favoritesSearchService.search().subscribe( res => {});
        return of(true);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }
}
