import { TestBed } from '@angular/core/testing';

import { FavoritesSearchService } from './favorites-search.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FavoritesSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
   }));

  it('should be created', () => {
    const service: FavoritesSearchService = TestBed.get(FavoritesSearchService);
    expect(service).toBeTruthy();
  });
});
