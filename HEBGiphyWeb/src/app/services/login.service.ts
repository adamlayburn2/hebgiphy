import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  // Local Session storage
  // For the limited use of this "coding challenge" I did not go so far as to add
  // a cookie with the sessionId however that is the normal way I do this.  That way
  // when you return to the application you skip the login page if your session is
  // still valid.  That said, for this demo project I simply made session ephemeral
  private _currentSession: BehaviorSubject<any> = new BehaviorSubject(null);
  public readonly currentSession: Observable<any> = this._currentSession.asObservable();

  // Local User storage
  private _currentUser: BehaviorSubject<any> = new BehaviorSubject(null);
  public readonly currentUser: Observable<any> = this._currentUser.asObservable();

  /*
  * Validate login credentials and establish a session
  */
  logIn(email: string, password: string):Observable<any> {
    return this.http.post<any>(`/api/login`, { email: email, password: password })
    .pipe(
       tap( res =>{
        this._currentSession.next(res);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
  * Get the current SessionID from the private BehaviorSubject
  */
  getSessionId(){
    if(this._currentSession && this._currentSession.value && this._currentSession.value.sessionid){
      return this._currentSession.value.sessionid;
    }
    return null;
  }

  /*
  * Check session validity in the backend
  */
  checkSession(){
    if(!this._currentSession.value){
      this._currentSession.next(null);
      return of(null);
    }
    return this.http.get<any>('/api/sessions/' + this._currentSession.value.sessionid)
    .pipe(
      tap(res => {
        this._currentSession.next(res);
        // valid so load the users info
        this.getCurrentUser(res['userid']).subscribe(user => {})
      }),
     catchError(err =>{
       return of(err);
     })
    )
  }

  /*
  * Get your user profile
  */
  getCurrentUser(id){
    return this.http.get<any>('/api/users/' + id)
    .pipe(
      tap(res => {
        this._currentUser.next(res);
      }),
      catchError(error => {
        this._currentUser.next(null);
        return of(error);
      })
    )
  }

  /*
  * Clear session and user info
  */
  logout(){
    this._currentUser.next(null);
    this._currentSession.next(null);
  }

  /*
  * Sometimes you want to log failures
  */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
