import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GiphySearchService {

  constructor(private http: HttpClient) { }

  private _searchResults: BehaviorSubject<any> = new BehaviorSubject(null);
  public readonly searchResults: Observable<any> = this._searchResults.asObservable();

  // this could be extracted to configuration but for now I left it here
  // it is not a secret since all clients would share the key
  private readonly apiKey: string = "uMoB5erYF2l5CHvjrN3Ae4kj0vyLlP4t";

  /*
  * Search GIPHY for either stickers or GIFs
  */
  search(text: string, searchType: string):Observable<any> {
    return this.http.get<any>('https://api.giphy.com/v1/' + searchType + '/search?api_key=' + this.apiKey + '&q=' + text + '&rating=g&limit=25')
    .pipe(
       tap( res =>{
        this._searchResults.next(res);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
  * Search GIPHY for either stickers or GIFs page N
  */
  nextPage(text: string, searchType: string):Observable<any> {
    let offset = this._searchResults.value.pagination.offset + this._searchResults.value.pagination.count;
    return this.http.get<any>('https://api.giphy.com/v1/' + searchType + '/search?api_key=' + this.apiKey + '&q=' + text + '&rating=g&offset='+offset)
    .pipe(
       tap( res =>{
        this._searchResults.next(res);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
  * Search GIPHY for either stickers or GIFs page N
  */
  prevPage(text: string, searchType: string):Observable<any> {
    let offset = this._searchResults.value.pagination.offset - this._searchResults.value.pagination.count;
    return this.http.get<any>('https://api.giphy.com/v1/' + searchType + '/search?api_key=' + this.apiKey + '&q=' + text + '&rating=g&offset='+offset)
    .pipe(
       tap( res =>{
        this._searchResults.next(res);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
  * Invalidate the last search
  */
  reset(){
    this._searchResults.next(null);
  }

  //Todo: return subsequent pages of the search results via the contents of the last _searchResults
}
