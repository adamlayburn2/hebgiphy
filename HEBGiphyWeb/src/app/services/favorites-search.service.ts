import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FavoritesSearchService {

  constructor(private http: HttpClient) { }

  private _favoritesResults: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public readonly favoritesResults: Observable<any[]> = this._favoritesResults.asObservable();

  /**
   * Return the GIFs saved to your profile
   */
  search():Observable<any> {
    return this.http.get<any>('/api/giphs')
    .pipe(
       tap( res =>{
        this._favoritesResults.next(res);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }

  /*
   * Search cleared
   */
  reset(){
    this._favoritesResults.next([]);
  }
}
