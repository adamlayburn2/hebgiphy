import { TestBed } from '@angular/core/testing';

import { GiphService } from './giph.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('GiphService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
   }));

  it('should be created', () => {
    const service: GiphService = TestBed.get(GiphService);
    expect(service).toBeTruthy();
  });
});
