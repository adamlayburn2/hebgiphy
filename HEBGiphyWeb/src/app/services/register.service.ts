import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  /*
  * Register a new login
  */
  register(first: string, last: string, email: string, password: string):Observable<any> {
    return this.http.post<any>(`/api/register`, { first: first, last: last, email: email, password: password })
    .pipe(
       tap( res =>{
        return of(res);
       }),
       catchError(err =>{
         return of(err);
       })
     );
  }
}
