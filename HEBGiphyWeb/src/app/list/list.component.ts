import { Component, OnInit } from '@angular/core';
import { GiphySearchService } from '../services/giphy-search.service';
import { FavoritesSearchService } from '../services/favorites-search.service';
import { GiphService } from '../services/giph.service';
import { MatSnackBar } from '@angular/material';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private giphySearchService: GiphySearchService, private favoritesSearchService: FavoritesSearchService,
              private giphService: GiphService, public snackBar: MatSnackBar) { }

  searchText: string;
  searchType: string = "gifs";
  viewSize: string = "photo_size_select_large"

  ngOnInit() {
    this.updateFavorites();
  }

  search(){
    // search GIPHY for either stickers or gifs
    this.giphySearchService.search(this.searchText, this.searchType).subscribe(res => { } );
  }

  nextPage(){
    this.giphySearchService.nextPage(this.searchText, this.searchType).subscribe(res => { } );
  }

  prevPage(){
    this.giphySearchService.prevPage(this.searchText, this.searchType).subscribe(res => { } );
  }

  clear(){
    this.searchText = '';
    this.giphySearchService.reset();
  }

  updateFavorites(){
    // load your favorited gifs
    this.favoritesSearchService.search().subscribe(res => { } );
  }

  /**
  * Add the GIF to your favorites
  */
  favorite(img){
    this.giphService.add(img.images.fixed_width.url).subscribe( res => {
      if(res && res.giphid){
        // mark as saved so the heart turns colors
        img.saved = true;
        this.snackBar.open("Added to your favorites!", "close", { duration: 2000, panelClass:['snackbar-success'] });
      }
      else{
        this.snackBar.open("Unable to add to your favorites :( ", "close", { duration: 2000, panelClass:['snackbar-error'] });
      }
    });
  }

  /*
  * Remove the GIF from your favorites
  */
  remove(img){
    this.giphService.delete(img.giphid).subscribe( res => {
      if(res && res.deletedon){
        this.snackBar.open("Removed", "close", { duration: 2000, panelClass:['snackbar-success'] });
      }
      else{
        this.snackBar.open("Unable to remove from your favorites :( ", "close", { duration: 2000, panelClass:['snackbar-error'] });
      }
    });
  }

  /*
  * Share the GIF by copying the URL to clipboard
  */
  share(img){
    this.copyToClipboard(img.giphurl);
    this.snackBar.open("GIF URL copied to clipboard! ", "close", { duration: 2000, panelClass:['snackbar-success'] });
  }

  /*
   * Remove the label from GIF
   */
  removeLabel(label){
    this.giphService.deleteLabel(label.labelid).subscribe( res => {
      if(res && res.labelid){
        this.snackBar.open("Label Removed! ", "close", { duration: 2000, panelClass:['snackbar-success'] });
      }
      else{
        this.snackBar.open("Unable to remove label :( ", "close", { duration: 2000, panelClass:['snackbar-error'] });
      }
    });
  }

  /*
   * Add a new label to GIF
   */
  addLabel(img): void {
    this.giphService.addLabel(img, img.labelText).subscribe( res => {
      if(res && res.labelid){
        this.snackBar.open("Label Added! ", "close", { duration: 2000, panelClass:['snackbar-success'] });
        img.labelText = "";
      }
      else{
        this.snackBar.open("Unable to add label :( ", "close", { duration: 2000, panelClass:['snackbar-error'] });
      }
    });
  }

  copyToClipboard(item): void {
      let listener = (e: ClipboardEvent) => {
          e.clipboardData.setData('text/plain', (item));
          e.preventDefault();
      };

      document.addEventListener('copy', listener);
      document.execCommand('copy');
      document.removeEventListener('copy', listener);
  }
}
