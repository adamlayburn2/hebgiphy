import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from '../services/login.service';
import { Observable, of } from 'rxjs';
import { first, map, catchError } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private loginService: LoginService ) { }

    /**
     *  Guard to check your session and send you to login if that session is invalid.  Only activated on certain routes.
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.loginService.checkSession()
          .pipe(
            map(res => {
             if(res){
              return true;
             }
             else{
              this.router.navigate(['login']);
              return false;
             }
         }),
         catchError(err => {
            this.router.navigate(['login']);
            return of(false);
         }));

    }
}
