import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable()
export class HEBInterceptor implements HttpInterceptor {

  constructor(private loginService: LoginService) {}

  /*
  * An interceptor to add the hebsession header to all local requests when a session is established
  */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var sessionId = this.loginService.getSessionId();

    // inject sessionId to any local route when a session is established
    if(sessionId && req.url.startsWith('/')){
      const modifiedReq = req.clone({
        headers: req.headers.set('hebsession', sessionId ),
      });
      return next.handle(modifiedReq);
    }

    // non injected
    return next.handle(req);
  }
}
