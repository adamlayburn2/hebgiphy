To save the reviewers some time, I am going to include everthing in one Repo.

Brief overview of the solution:

1) The backend is a .NET Rest API that provides the following endpoints

- POST /api/register

- POST /api/login

- GET /sessions/{id}

- GET /users/{id}              [restricted]

- GET /api/giphs               [restricted]

- GET /api/giphs/{id}          [restricted]

- POST /api/giphs              [restricted]

- DELETE /api/giphs            [restricted]

- POST /api/labels             [restricted]

- DELETE /api/labels/{id}      [restricted]

2) The frontend is an Angular 7 web application

- ng will reverse proxy the endpoints at /api/ to those served by the backend so that we do not need to add CORS headers.

3) Security/Session details

- For the sake of simplicity I went with an ephemeral authorization model.  Your session is active while you use the application but when you close the browser or naviate away you will need to establish a new session.  Normally I would store your auth token in a cookie so that anytime you return to the application, you remain logged in -- until you session is expired or removed from the backend.  Normally sessions would be expired and removed after some amount of aging by the backend.

- Data in the backend follows a strict ownership model.  All endpoints restrict result data to *only* that which you created.

---

## Data

1) Install a local instance of Postgres.  I'm using PostgreSQL 10 with pgAdmin.

2) Create a database named 'hebgiphy'

3) Create a user 'heb'

4) Run the create script found in the Data folder. 

---

## Backend

1) Open the .NET solution 'HEBGiphy.sln'

2) Build and run

3) You should see the backend running at 'https://localhost:5001/api'

---

## Frontend

1) Navigate to HEBGiphyWeb in a terminal

2) run 'npm install'

3) run 'ng serve --proxy-config proxy-config.js'

4) Open a browser and navigate to 'http://localhost:4200/'

---

## Testing

1) Backend endpoints were tested using Postman so I will export and add that to the project as well.  Certainly, unit testing can also be done via xUnit.net tests.

2) Frontend conponents were scaffolded with Karma/Jasmin tests and I'll spend some time writing a few tests to provide examples on what some of them would look like.

